using Microsoft.AspNetCore.Mvc;

namespace ShareAPI.Controllers
{
  /*  [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static  string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
        [HttpGet]
        [Route("byNumberOfRecords")]
        public IEnumerable<WeatherForecast> Get(int numberOfRecords)
        {
            return Enumerable.Range(1, numberOfRecords).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost] //especificar es importante debido a que si no se pone puede accesarse desde cualquier verbo put post delete etc..
        public void Post([FromForm]string name)
        {
            Summaries = Summaries.Append(name).ToArray();

           
        }

        [HttpDelete] //especificar es importante debido a que si no se pone puede accesarse desde cualquier verbo put post delete etc..
        public void Delete( long id) { 


           
            Summaries = Summaries.Where((source, index) => index != id).ToArray();


        }
    }*/
}