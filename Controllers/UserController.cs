using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ShareAPI.Services;

namespace ShareAPI.Controllers
{
    [EnableCors("MYPOLICY")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserDataService _userDataService;
        public UserController(IUserDataService userDataService)
        {
            _userDataService = userDataService;
        }
     
        [HttpGet]
        [HttpPost] //AMBOS VERBOS FUNCIONAN, si se quitan ambos funciona con todo los verbos
        public ActionResult<IEnumerable<string>> Get([FromServices] IUserDataService DataService)
        {
            return _userDataService.GetElements().Union(DataService.GetElements()).ToList();

       
            //return Ok(new string[] { "value1", "value2" });
        }

        [HttpGet("{id}")]//si se quita {id} no es obligatorio el cmapo sin embargo daria un error por haber dos get habria que quitar uno
        public ActionResult<string> Get(int id)
        {
            if(id>0){ //emular un error
                return "value";
            }
            else if(id<0){
                return BadRequest();
            }
            
            else {
                return NotFound();
            }
        }

       /* [HttpPost]
        public void Post([FromBody] string value)
        {
        }
        */
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}