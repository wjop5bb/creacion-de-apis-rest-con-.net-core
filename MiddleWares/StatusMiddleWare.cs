using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareAPI.MiddleWares
{
    public class StatusMiddleWare
    {
        readonly RequestDelegate next;


        public StatusMiddleWare(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            //await context.Response.WriteAsync("Hello World2!");
            //despues de esto se ejecuta el next
            await next(context);
        }
    }

    public static class StatusMiddleWareExtensions
    {
        public static IApplicationBuilder UseStatusMiddleWare(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<StatusMiddleWare>();
        }
    }
}