using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareAPI.Services
{
    public class UserDataService : IUserDataService
    {

        private List<string> Elements;
        public UserDataService()
        {   Elements = new List<string>();
            var rnd = new Random();
            Elements.Add($"Valor {rnd.Next()}");
            Elements.Add($"Valor {rnd.Next()}");
        }
        public List<string> GetElements()
        {
            return Elements;
        }
    }
}