using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareAPI.Services
{
    public interface IUserDataService
    {
        List<string> GetElements();
    }
}