

using ShareAPI.Services;
using ShareAPI.MiddleWares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.



builder.Services.AddControllers();
builder.Services.AddTransient<IUserDataService, UserDataService>();//si queremos que la info perdure usamos AddSingleton y AddTransient es mas estricto que AddScoped porque crea una instancia nueva cada vez que se llama a la funcion
builder.Services.AddCors(options =>
{
    options.AddPolicy("MYPOLICY",
                      builder =>
                      {
                          builder.AllowAnyHeader()
      .AllowAnyOrigin()//si queremos que todos los dominios puedan acceder a nuestra api | WithOrigins("http://localhost:4200") si especifiamos solo un dominio
      .WithMethods("GET", "POST", "PUT", "DELETE").Build();
                      });
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
app.UseCors("MYPOLICY"/* builder =>
{
   builder.AllowAnyHeader()
    .AllowAnyOrigin()//si queremos que todos los dominios puedan acceder a nuestra api | WithOrigins("http://localhost:4200") si especifiamos solo un dominio
    .WithMethods("GET", "POST", "PUT", "DELETE").Build();

}*/);
app.UseAuthorization();

app.MapControllers();

app.MapGet("/MyRoot", async context =>
 {
     await context.Response.WriteAsync("Hello World!");
 });
app.UseStatusMiddleWare();

/*
app.Run(async context =>
{
    await context.Response.WriteAsync("URL NO ENCONTRADA");
});*/
app.Run();

